<?php

declare(strict_types=1);

require __DIR__ . '/../vendor/autoload.php';

/** @var resource $fh */
$fh = fopen(__DIR__ . '/example.txt', 'r');

/** @var \App\Parser\Parser $parser */
$parser = new \App\Parser\Parser();

/** @var \App\Model\Result $result */
foreach ($parser->parse($fh) as $result) {
    /** @var \App\Model\Point $averaged */
    $averaged = $result->getAveragedPoint();

    printf(
        "%s %s %s\n",
        round($averaged->getX(), 4),
        round($averaged->getY(), 4),
        round($result->getMaxDistance(), 5)
    );
}

fclose($fh);
