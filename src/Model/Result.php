<?php

declare(strict_types=1);

namespace App\Model;

/**
 * Class Result
 * @package App\Model
 */
class Result
{
    /**
     * @var Point
     */
    private $averagedPoint;

    /**
     * @var float
     */
    private $maxDistance;

    /**
     * Result constructor.
     * @param Point $averagedPoint
     * @param float $maxDistance
     */
    public function __construct(Point $averagedPoint, float $maxDistance)
    {
        $this->averagedPoint = $averagedPoint;
        $this->maxDistance = $maxDistance;
    }

    /**
     * @param Point $averagedPoint
     */
    public function setAveragedPoint(Point $averagedPoint): void
    {
        $this->averagedPoint = $averagedPoint;
    }

    /**
     * @return Point
     */
    public function getAveragedPoint(): Point
    {
        return $this->averagedPoint;
    }

    /**
     * @param float $maxDistance
     */
    public function setMaxDistance(float $maxDistance): void
    {
        $this->maxDistance = $maxDistance;
    }

    /**
     * @return float
     */
    public function getMaxDistance(): float
    {
        return $this->maxDistance;
    }
}