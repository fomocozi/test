<?php

declare(strict_types=1);

namespace App\Model;

/**
 * Class Visitor
 * @package App\Model
 */
class Visitor
{
    /**
     * @var Point
     */
    private $point;

    /**
     * @var float
     */
    private $angle;

    /**
     * PointVisitor constructor.
     * @param Point $point
     * @param float $angle
     */
    public function __construct(Point $point, float $angle = 0)
    {
        $this->point = $point;
        $this->angle = $angle;
    }

    /**
     * @param Point $point
     */
    public function setPoint(Point $point): void
    {
        $this->point = $point;
    }

    /**
     * @return Point
     */
    public function getPoint(): Point
    {
        return $this->point;
    }

    /**
     * @param float $angle
     */
    public function setAngle(float $angle): void
    {
        $this->angle = $angle;
    }

    /**
     * @return float
     */
    public function getAngle(): float
    {
        return $this->angle;
    }
}