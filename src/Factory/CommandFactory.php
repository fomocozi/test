<?php

declare(strict_types=1);

namespace App\Factory;

use App\Strategy;

/**
 * Class CommandFactory
 * @package App\Strategy
 */
class CommandFactory
{
    /**
     * @param string $name
     * @return Strategy\CommandInterface
     */
    public static function create(string $name): Strategy\CommandInterface
    {
        switch ($name) {
            case 'start':
                return new Strategy\CommandStart();

            case 'turn':
                return new Strategy\CommandTurn();

            case 'walk':
                return new Strategy\CommandWalk();

            default:
                throw new \InvalidArgumentException(sprintf('Unknown command name "%s".', $name));
        }
    }
}