<?php

declare(strict_types=1);

namespace App\Parser;

use App\Factory\CommandFactory;
use App\Helper\GeoHelper;
use App\Model;
use App\Strategy\CommandInterface;

/**
 * Class Parser
 * @package App\Parser
 */
class Parser
{
    /**
     * @var float
     */
    private $countOfItems = 0;

    /**
     * @var float
     */
    private $sumOfX = 0;

    /**
     * @var float
     */
    private $sumOfY = 0;

    /**
     * @var Model\Point[]
     */
    private $points = [];

    /**
     * @param $stream
     * @return \Generator
     */
    public function parse($stream): \Generator
    {
        if (!is_resource($stream)) {
            throw new \InvalidArgumentException('Argument must be a valid resource.');
        }

        /** @var bool|string $line */
        while (($line = fgets($stream))) {
            if (preg_match('/^(\d+)$/', $line, $matchOfCount)) {
                if ($this->hasResult()) {
                    yield $this->createResult();
                }

                $this->resetState((int)$matchOfCount[1]);
            }

            if (
                !$this->countOfItems

                // The value must much as "10.1 20 ..."
                || !preg_match('/^(\-?[\d\.]+)\s+(\-?[\d\.]+)\s+(.+)$/', $line, $matchOfLine)

                // The value must much as "... command 10.1 other_command -20 ..."
                || !preg_match_all('/(\w+)\s+(\-?[\d\.]+)/', $matchOfLine[3], $matchesOfCommands, PREG_SET_ORDER)
            ) {
                continue;
            }

            /** @var Model\Point $point */
            $point = new Model\Point((float)$matchOfLine[1], (float)$matchOfLine[2]);

            /** @var Model\Visitor $visitor */
            $visitor = new Model\Visitor($point);

            foreach ($matchesOfCommands as list(, $name, $param)) {
                /** @var CommandInterface $command */
                $command = CommandFactory::create($name);
                $command->execute($visitor, (float)$param);
            }

            $this->sumOfX += $point->getX();
            $this->sumOfY += $point->getY();
            $this->points[] = $point;
        }

        if ($this->hasResult()) {
            yield $this->createResult();
        }
    }

    /**
     * @return Model\Result
     */
    private function createResult(): Model\Result
    {
        /** @var float $maxDistance */
        $maxDistance = 0;

        /** @var Model\Point $averaged */
        $averaged = new Model\Point($this->sumOfX / $this->countOfItems, $this->sumOfY / $this->countOfItems);

        /** @var Model\Point $point */
        foreach ($this->points as $point) {
            /** @var float $distance */
            $distance = GeoHelper::distance($averaged, $point);

            if ($distance > $maxDistance) {
                $maxDistance = $distance;
            }
        }

        return new Model\Result($averaged, $maxDistance);
    }

    /**
     * @param int $countOfItems
     */
    private function resetState(int $countOfItems): void
    {
        $this->countOfItems = $countOfItems;
        $this->sumOfX = 0;
        $this->sumOfY = 0;
        $this->points = [];
    }

    /**
     * @return bool
     */
    private function hasResult(): bool
    {
        return count($this->points) > 0;
    }
}
