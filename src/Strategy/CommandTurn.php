<?php

declare(strict_types=1);

namespace App\Strategy;

use App\Model\Visitor;

/**
 * Class CommandTurn
 * @package App\Strategy
 */
class CommandTurn implements CommandInterface
{
    /**
     * {@inheritdoc}
     */
    public function execute(Visitor $visitor, float $value): void
    {
        $visitor->setAngle($visitor->getAngle() + $value);
    }
}