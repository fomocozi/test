<?php

declare(strict_types=1);

namespace App\Strategy;

use App\Model\Point;
use App\Model\Visitor;

/**
 * Class CommandWalk
 * @package App\Strategy
 */
class CommandWalk implements CommandInterface
{
    /**
     * {@inheritdoc}
     */
    public function execute(Visitor $visitor, float $value): void
    {
        /** @var Point $point */
        $point = $visitor->getPoint();

        $point->setX($point->getX() + cos($visitor->getAngle() * pi() / 180) * $value);
        $point->setY($point->getY() + sin($visitor->getAngle() * pi() / 180) * $value);
    }
}