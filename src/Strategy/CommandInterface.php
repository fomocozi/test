<?php

declare(strict_types=1);

namespace App\Strategy;

use App\Model\Visitor;

/**
 * Interface CommandInterface
 * @package App\Strategy
 */
interface CommandInterface
{
    /**
     * @param Visitor $visitor
     * @param float $value
     */
    public function execute(Visitor $visitor, float $value): void;
}