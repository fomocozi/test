<?php

declare(strict_types=1);

namespace App\Strategy;

use App\Model\Visitor;

/**
 * Class CommandStart
 * @package App\Strategy
 */
class CommandStart implements CommandInterface
{
    /**
     * {@inheritdoc}
     */
    public function execute(Visitor $visitor, float $value): void
    {
        $visitor->setAngle($value);
    }
}