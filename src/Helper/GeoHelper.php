<?php

declare(strict_types=1);

namespace App\Helper;

use App\Model\Point;

/**
 * Class GeoHelper
 * @package App\Helper
 */
class GeoHelper
{
    /**
     * @param Point $start
     * @param Point $end
     * @return float
     */
    public static function distance(Point $start, Point $end): float
    {
        /** @var float $x */
        $x = $end->getX() - $start->getX();

        /** @var float $y */
        $y = $end->getY() - $start->getY();

        return sqrt($x * $x + $y * $y);
    }
}